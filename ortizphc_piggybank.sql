-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 09, 2021 at 12:58 AM
-- Server version: 10.3.27-MariaDB-cll-lve
-- PHP Version: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ortizphc_piggybank`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `id` int(11) NOT NULL,
  `account_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`id`, `account_name`) VALUES
(1, 'dhan-che');

-- --------------------------------------------------------

--
-- Table structure for table `banks`
--

CREATE TABLE `banks` (
  `id` varchar(20) NOT NULL,
  `account_fk` int(11) NOT NULL,
  `bank_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `banks`
--

INSERT INTO `banks` (`id`, `account_fk`, `bank_name`) VALUES
('000041054857', 1, 'BDO-SA-4857'),
('2979244188', 1, 'BPI-SA-4188');

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

CREATE TABLE `options` (
  `option_id` int(11) NOT NULL,
  `option_name` varchar(20) NOT NULL,
  `option_value` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `options`
--

INSERT INTO `options` (`option_id`, `option_name`, `option_value`) VALUES
(1, 'target_date', '2022-01-21');

-- --------------------------------------------------------

--
-- Table structure for table `savings`
--

CREATE TABLE `savings` (
  `id` int(11) NOT NULL,
  `amount` float NOT NULL,
  `date` date NOT NULL,
  `user` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp(),
  `bank_fk` varchar(20) NOT NULL,
  `type_fk` int(11) NOT NULL DEFAULT 1,
  `remarks` varchar(250) DEFAULT NULL,
  `is_void` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `savings`
--

INSERT INTO `savings` (`id`, `amount`, `date`, `user`, `timestamp`, `bank_fk`, `type_fk`, `remarks`, `is_void`) VALUES
(1, 3000, '2019-09-13', 1, '2019-09-13 02:58:32', '000041054857', 1, NULL, 0),
(2, 2000, '2019-09-13', 2, '2019-09-13 05:36:07', '000041054857', 1, NULL, 0),
(3, 2000, '2019-09-28', 2, '2019-09-26 20:07:04', '000041054857', 1, NULL, 0),
(4, 3000, '2019-09-28', 1, '2019-09-28 07:46:45', '000041054857', 1, NULL, 0),
(5, 3000, '2019-10-11', 1, '2019-10-11 03:11:21', '000041054857', 1, NULL, 0),
(6, 2000, '2019-10-16', 2, '2019-10-17 00:54:51', '000041054857', 1, NULL, 0),
(7, 3000, '2019-10-30', 1, '2019-11-11 14:35:13', '000041054857', 1, NULL, 0),
(8, 9000, '2019-11-13', 2, '2019-11-13 03:42:03', '000041054857', 1, NULL, 0),
(9, 1000, '2019-11-14', 2, '2019-11-14 04:48:55', '000041054857', 1, NULL, 0),
(10, 3000, '2019-11-14', 1, '2019-11-14 04:50:04', '000041054857', 1, NULL, 0),
(11, 1500, '2019-11-29', 2, '2019-11-29 23:46:15', '000041054857', 1, NULL, 0),
(12, 10000, '2019-12-02', 1, '2019-12-02 05:56:36', '000041054857', 1, NULL, 0),
(13, 3000, '2019-12-15', 1, '2020-01-01 04:33:10', '000041054857', 1, NULL, 0),
(14, 4500, '2019-12-30', 1, '2020-01-01 04:33:48', '000041054857', 1, NULL, 0),
(15, 10000, '2020-01-15', 1, '2020-01-15 10:21:42', '000041054857', 1, NULL, 0),
(16, 4000, '2020-01-16', 2, '2020-01-16 05:38:44', '000041054857', 1, NULL, 0),
(17, 3000, '2020-01-30', 1, '2020-01-30 05:01:35', '000041054857', 1, NULL, 0),
(18, 2000, '2020-02-01', 2, '2020-02-01 06:08:05', '000041054857', 1, NULL, 0),
(19, 3000, '2020-02-13', 1, '2020-02-17 01:29:50', '000041054857', 1, NULL, 0),
(20, 2000, '2020-02-14', 1, '2020-02-17 01:30:14', '000041054857', 1, NULL, 0),
(21, 3000, '2020-02-27', 1, '2020-02-28 00:22:52', '000041054857', 1, NULL, 0),
(22, 3000, '2020-03-02', 2, '2020-03-09 22:26:27', '000041054857', 1, NULL, 0),
(23, 3000, '2020-03-15', 1, '2020-03-19 01:51:05', '000041054857', 1, NULL, 0),
(24, 3000, '2020-03-15', 2, '2020-03-19 01:51:22', '000041054857', 1, NULL, 0),
(25, 2000, '2020-03-28', 2, '2020-03-28 04:39:03', '000041054857', 1, NULL, 0),
(26, 3000, '2020-03-28', 1, '2020-03-28 04:39:17', '000041054857', 1, NULL, 0),
(27, 3500, '2020-04-11', 2, '2020-04-11 02:59:30', '000041054857', 1, NULL, 0),
(28, 3000, '2020-04-14', 1, '2020-04-14 13:14:18', '000041054857', 1, NULL, 0),
(29, 2500, '2020-04-29', 1, '2020-04-29 06:44:46', '000041054857', 1, NULL, 0),
(31, 3000, '2020-04-30', 2, '2020-04-30 03:34:42', '2979244188', 1, NULL, 0),
(33, 6000, '2020-05-15', 2, '2020-05-15 02:31:34', '2979244188', 1, NULL, 0),
(34, 5000, '2020-05-17', 1, '2020-05-17 09:19:30', '2979244188', 1, NULL, 0),
(35, 3000, '2020-05-29', 2, '2020-05-28 19:49:43', '2979244188', 1, NULL, 0),
(36, 5000, '2020-05-30', 1, '2020-06-08 00:09:14', '2979244188', 1, NULL, 0),
(37, 10000, '2020-06-13', 2, '2020-06-13 06:06:14', '2979244188', 1, NULL, 0),
(38, 3000, '2020-06-15', 1, '2020-06-15 06:39:48', '2979244188', 1, NULL, 0),
(39, 6000, '2020-06-30', 2, '2020-07-01 15:50:49', '2979244188', 1, NULL, 0),
(40, 2000, '2020-07-15', 2, '2020-07-15 02:51:09', '2979244188', 1, NULL, 0),
(41, 1500, '2020-07-31', 2, '2020-07-30 18:38:45', '2979244188', 1, NULL, 0),
(42, 2500, '2020-08-14', 2, '2020-08-14 03:51:01', '2979244188', 1, NULL, 0),
(43, 3000, '2020-08-25', 1, '2020-08-25 05:10:35', '2979244188', 1, NULL, 0),
(44, 2000, '2020-08-30', 2, '2020-08-29 23:39:40', '2979244188', 1, NULL, 0),
(45, 3000, '2020-09-05', 1, '2020-09-05 21:15:19', '2979244188', 1, NULL, 0),
(46, 2000, '2020-09-16', 2, '2020-09-16 09:50:35', '2979244188', 1, NULL, 0),
(47, 3500, '2020-10-01', 2, '2020-09-30 20:12:30', '2979244188', 1, NULL, 0),
(48, 3000, '2020-10-01', 1, '2020-10-08 06:43:28', '2979244188', 1, NULL, 0),
(50, 2000, '2020-10-15', 2, '2020-10-15 14:11:46', '2979244188', 1, NULL, 0),
(51, 3000, '2020-10-15', 1, '2020-10-15 15:58:40', '2979244188', 1, NULL, 0),
(52, 3000, '2020-10-30', 2, '2020-10-30 01:57:33', '2979244188', 1, NULL, 0),
(54, 3000, '2020-11-03', 1, '2020-11-03 11:16:40', '2979244188', 1, NULL, 0),
(55, 2000, '2020-11-13', 2, '2020-11-13 07:42:01', '2979244188', 1, NULL, 0),
(56, 3000, '2020-11-25', 1, '2020-11-25 04:05:55', '2979244188', 1, NULL, 0),
(57, 11500, '2020-11-29', 2, '2020-11-29 04:43:00', '2979244188', 1, NULL, 0),
(58, 30000, '2020-12-07', 1, '2020-12-09 16:35:45', '2979244188', 2, '30,010 Retainer fee to JEM for wedding package + 10 service charge', 0),
(59, 10000, '2020-12-14', 1, '2020-12-14 01:14:46', '2979244188', 1, NULL, 0),
(60, 3000, '2020-12-16', 2, '2020-12-15 17:04:31', '2979244188', 1, NULL, 0),
(61, 2000, '2020-12-30', 2, '2020-12-29 18:35:10', '2979244188', 1, NULL, 0),
(62, 10000, '2021-01-12', 2, '2021-01-12 00:59:12', '2979244188', 1, NULL, 0),
(63, 4000, '2021-01-14', 1, '2021-01-14 05:19:15', '2979244188', 1, NULL, 0),
(64, 5000, '2021-01-14', 1, '2021-01-14 05:21:59', '000041054857', 2, '5,000 payment to LifePlace for reservation', 0),
(65, 5025, '2021-01-15', 1, '2021-01-15 02:04:22', '000041054857', 2, '5,000 payment to Caleruega for reservation + 25 service charge', 0),
(66, 1000, '2021-01-17', 2, '2021-01-17 03:06:35', '2979244188', 1, NULL, 0),
(67, 2000, '2021-01-23', 1, '2021-01-23 08:35:39', '000041054857', 2, '2,000 withdraw from BDO for ocular visit', 0),
(68, 2000, '2021-01-28', 1, '2021-01-28 03:08:40', '2979244188', 1, NULL, 0),
(69, 2000, '2021-01-28', 1, '2021-01-28 03:09:57', '000041054857', 1, NULL, 0),
(70, 1000, '2021-02-01', 2, '2021-02-01 03:40:07', '2979244188', 1, NULL, 0),
(71, 4000, '2021-02-14', 1, '2021-02-14 04:06:28', '2979244188', 1, NULL, 0),
(72, 3000, '2021-02-14', 2, '2021-02-14 04:06:52', '2979244188', 1, NULL, 0),
(73, 4000, '2021-02-27', 1, '2021-02-27 06:56:37', '2979244188', 1, NULL, 0),
(74, 3000, '2021-02-27', 2, '2021-02-27 06:57:03', '2979244188', 1, NULL, 0),
(75, 30010, '2021-03-02', 1, '2021-03-04 03:08:23', '2979244188', 2, '30,000 PARTIAL PAYMENT TO JEM FOR WEDDING PACKAGE\r\n+ 10 SERVICE CHARGE', 0),
(76, 23010, '2021-03-03', 1, '2021-03-04 03:09:32', '2979244188', 2, '23,000 PARTIAL PAYMENT TO JEM FOR WEDDING PACKAGE + 10 SERVICE CHARGE', 0),
(77, 1410, '2021-03-04', 1, '2021-03-04 03:12:00', '2979244188', 2, '1,400 PARTIAL PAYMENT TO 206 STUDIO + 10 SERVICE CHARGE', 0),
(78, 8045, '2021-03-04', 1, '2021-03-04 03:25:18', '2979244188', 2, '8045.60 CEBU PACIFIC AIRFARE FROM LEG TO MLA VV FOR ORTIZ FAMILY', 0),
(79, 4000, '2021-03-13', 2, '2021-03-13 04:08:50', '2979244188', 1, NULL, 0),
(80, 4010, '2021-03-20', 2, '2021-03-20 04:34:15', '2979244188', 2, '4,000 PAYMENT FOR PRE-CANA + 10 SERVICE CHARGE', 0),
(82, 3000, '2021-04-18', 1, '2021-04-18 13:27:06', '2979244188', 1, NULL, 0),
(83, 6000, '2021-04-18', 2, '2021-04-18 13:27:54', '2979244188', 1, NULL, 0),
(84, 2000, '2021-05-03', 2, '2021-05-03 00:33:29', '2979244188', 1, NULL, 0),
(85, 3000, '2021-05-03', 1, '2021-05-03 00:35:24', '2979244188', 1, NULL, 0),
(86, 5000, '2021-05-29', 1, '2021-05-28 16:34:02', '2979244188', 1, NULL, 0),
(87, 2000, '2021-05-29', 2, '2021-05-28 16:34:25', '2979244188', 1, NULL, 0),
(88, 3000, '2021-06-15', 2, '2021-06-14 17:08:41', '2979244188', 1, NULL, 0),
(89, 2000, '2021-06-15', 1, '2021-07-03 03:06:32', '2979244188', 1, NULL, 0),
(90, 2000, '2021-06-29', 1, '2021-07-03 03:06:46', '2979244188', 1, NULL, 0),
(91, 2500, '2021-07-15', 2, '2021-07-14 16:38:14', '2979244188', 1, NULL, 0),
(92, 10000, '2021-07-29', 1, '2021-07-29 13:55:50', '2979244188', 1, NULL, 0),
(93, 2000, '2021-07-29', 2, '2021-07-29 13:56:36', '2979244188', 1, NULL, 0),
(94, 3000, '2021-08-13', 2, '2021-08-13 08:26:19', '2979244188', 1, NULL, 0),
(95, 2000, '2021-08-28', 2, '2021-08-28 13:44:34', '2979244188', 1, NULL, 0),
(96, 10000, '2021-09-01', 1, '2021-09-01 11:02:11', '2979244188', 1, NULL, 0),
(97, 10000, '2021-10-05', 1, '2021-10-05 12:58:22', '2979244188', 1, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `transaction_types`
--

CREATE TABLE `transaction_types` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaction_types`
--

INSERT INTO `transaction_types` (`id`, `name`, `description`) VALUES
(1, 'deposit', 'cash deposit'),
(2, 'withdrawal', 'cash withdrawal');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `account_id`, `name`, `email`, `password`) VALUES
(1, 1, 'Dhan', 'mckentash3426@gmail.com', 'sakuranodaimon'),
(2, 1, 'Che', 'chejanoras@yahoo.com', 'chedhan.121291');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banks`
--
ALTER TABLE `banks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_fk` (`account_fk`);

--
-- Indexes for table `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`option_id`);

--
-- Indexes for table `savings`
--
ALTER TABLE `savings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type_fk` (`type_fk`);

--
-- Indexes for table `transaction_types`
--
ALTER TABLE `transaction_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `options`
--
ALTER TABLE `options`
  MODIFY `option_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `savings`
--
ALTER TABLE `savings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;

--
-- AUTO_INCREMENT for table `transaction_types`
--
ALTER TABLE `transaction_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `banks`
--
ALTER TABLE `banks`
  ADD CONSTRAINT `banks_ibfk_1` FOREIGN KEY (`account_fk`) REFERENCES `accounts` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

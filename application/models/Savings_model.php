<?php
class Savings_model extends CI_Model {

    function addSaving() {
		$data = array(
			'amount' => $this->input->post('amount'),
			'date' => $this->input->post('date'),
			'user' => $this->session->userdata('id'),
			'bank_fk' => $this->input->post('bank')
		);
		$this->db->insert('savings', $data);          
		return 0;
	}

	function addTransaction() {
		$data = array(
			'amount' => $this->input->post('amount'),
			'date' => $this->input->post('date'),
			'user' => $this->session->userdata('id'),
			'bank_fk' => $this->input->post('bank'),
			'type_fk' => $this->input->post('type'),
			'remarks' => $this->input->post('remarks')
		);
		$this->db->insert('savings', $data);          
		return 0;
	}

	function getBanks() {
		return $this->db->get('banks')->result_array();
	}

	function getTransactionTypes() {
		return $this->db->get('transaction_types')->result_array();
	}

	function getSavings($user=null) {
		$this->db->select('savings.*, users.name, banks.bank_name');
		$this->db->from('savings');
		$this->db->where('savings.type_fk', 1);
		$this->db->join('users', 'users.id = savings.user');
		$this->db->join('banks', 'banks.id = savings.bank_fk');
		// $this->db->join('transaction_types', 'transaction_types.id = savings.type_fk');
		$this->db->join('accounts', 'accounts.id = users.account_id');
		$query = $this->db->get();
		$result = $query->result_array();
		
		return $result;
	}
	
	function getTransactions($user=null) {
		$this->db->select('savings.*, users.name as user, banks.bank_name, transaction_types.name as type');
		$this->db->from('savings');
		$this->db->where('savings.is_void', 0);
		$this->db->join('users', 'users.id = savings.user');
		$this->db->join('banks', 'banks.id = savings.bank_fk');
		$this->db->join('transaction_types', 'transaction_types.id = savings.type_fk');
		$this->db->join('accounts', 'accounts.id = users.account_id');
		$this->db->order_by('savings.timestamp', 'ASC');
		$query = $this->db->get();
		$result = $query->result_array();
		
		return $result;
	}

	function getTotalSavings($user) {
		$this->db->select('*');
		$this->db->from('savings');
		$this->db->join('users', 'users.id = savings.user');
		$this->db->where('users.id', $user); 
		$query = $this->db->get();
		$results = $query->result_array();
		$total_amount = 0;
		foreach ($results as $result) {
			$total_amount += $result['amount'];
		}
		return $total_amount;
	}

	function getSumAllSavings() {
		return $this->db->select_sum('amount')->from('savings')->where('type_fk', 1)->get()->row_array()['amount'];
	}

	function getSumAllWithdrawals() {
		return $this->db->select_sum('amount')->from('savings')->where('type_fk', 2)->get()->row_array()['amount'];
	}

	function getMonthElapse() {
		$date_first = $this->db->select('date')->from('savings')->order_by('date', 'ASC')->get()->row_array()['date'];
		$date_last = $this->db->select('date')->from('savings')->order_by('date', 'DESC')->get()->row_array()['date'];
		$interval_year = date_diff(date_create($date_first), date_create($date_last))->format('%y');
		$interval_month = date_diff(date_create($date_first), date_create($date_last))->format('%m');
	
		return array(
				'date_first' => $date_first,
				'date_last' => $date_last,
				'month_elapse' => ($interval_year * 12) + $interval_month
			);
	}

	function getOptions() {
		return $this->db->get('options')->result_array();
	}

	function getAccountUsers($account_id) {
		return $this->db->select("users.*")->from('accounts')
		->where('accounts.id', $account_id)
		->join('users', 'users.account_id = accounts.id')
		->get()->result_array();
	}

	function getBankAccounts() {
		$banks = $this->db->get('banks')->result_array();
		print_r($banks);
		
		foreach ($banks as $bid => $b) {
			$deposit = $this->db->select_sum('amount')
				->from('savings')
				->where('bank_fk', $b['id'])
				->get()
				->row_array();


			print_r($deposit);
		}
		
		

		die;
	}

}

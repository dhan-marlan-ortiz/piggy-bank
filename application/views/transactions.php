<div class="inner-page">   
   <div class="container">
      <!-- Add new saving -->
      <div class="max-width-xs">
         
      </div>
      <!-- Add new saving end -->
   </div>
   <div class="container">
      <div class="row">
         <div class="col-md-4">
            <div class="panel">
               <div class="panel-heading">
                  <h3 class="panel-title">
                     Add Record
                  </h3>
               </div>
               <div class="panel-content">
                  <form method="POST" action="Transactions">
                     <div class="">
                        <div class="">
                            <label for="">Amount</label>
                           <input type="number" id="amount" name="amount" placeholder="Amount" required>
                        </div>
                        <div class="">
                            <label for="">Date</label>
                           <input type="date" id="date" name="date" placeholder="Date" value="<?php echo date('Y-m-d'); ?>"
                              required>
                        </div>
                        <div class="">
                            <label for="">Transaction Type</label>
                           <select name="type" id="" required class="text-capitalize">
                              <option value="" selected disabled>Select Type</option>
                              <?php 
                                    foreach ($transaction_types as $type) {
                                          echo "<option class='text-capitalize' value='" . $type['id'] . "'>" . $type['name'] . "</option>";
                                    }
                              ?>
                           </select>
                        </div>
                        <div class="">
                            <label for="">Bank</label>
                           <select name="bank" id="" required>
                              <option value="" selected disabled>Select Bank</option>
                              <?php 
                                    foreach ($banks as $bank) {
                                          echo "<option value='" . $bank['id'] . "'>" . $bank['bank_name'] . "</option>";
                                    }
                              ?>
                           </select>
                        </div>
                        <div class="">
                            <label for="">Remarks</label>
                            <textarea name="remarks" id="" rows="5"></textarea>
                        </div>                        
                        <div class="">
                           <input type="submit" id="save" name="save" value="Save">                           
                        </div>                        
                     </div>
                  </form>
               </div>
            </div>
            
            <?php 
               $total_savings = $getSumAllSavings;
               $month_elapsed =  $getMonthElapse['month_elapse'];
               $average_savings = $total_savings / $month_elapsed;
               $target_date = $getOptions[0]['option_value'];

               $cd_year = date_diff(date_create($target_date), date_create(date('Y-m-d')))->format('%y');
               $cd_month = date_diff(date_create($target_date), date_create(date('Y-m-d')))->format('%m');
               $cd_day = date_diff(date_create($target_date), date_create(date('Y-m-d')))->format('%d');
               $cd_in_months = (intval($cd_year) * 12) + intval($cd_month);
            ?>
            <div class="panel">
               <div class="panel-heading">
                  <h2 class="panel-title">Summary</h2>
               </div>
               <div class="panel-content">
                  <table class="table">
                        <tbody>
                           <tr>
                                 <th class="text-left">First entry</th>
                                 <td class="text-right">
                                       <?php echo date("M d, Y",strtotime($getMonthElapse['date_first'])); ?>
                                 </td>
                           </tr>
                           <tr>
                                 <th class="text-left">Last entry</th>
                                 <td class="text-right">
                                       <?php echo date("M d, Y",strtotime($getMonthElapse['date_last'])); ?>
                                 </td>
                           </tr>
                           <tr>
                                 <th class="text-left">Months</th>
                                 <td class="text-right">
                                       <?php echo $month_elapsed; ?>
                                 </td>
                           </tr>
                           <tr>
                                 <th class="text-left">Target Date</th>
                                 <td class="text-right">

                                       <?php //echo $getOptions['target_date'][];
                                          echo date("M d, Y",strtotime($target_date));
                                       ?>
                                 </td>
                           </tr>
                           <tr>
                                 <th class="text-left">Countdown</th>
                                 <td class="text-right">
                                       <?php 
                                          
                                          echo $cd_year . " Year, " . $cd_month . ($cd_month > 1 ? ' Months, ' : ' Month, ') . $cd_day . ($cd_month > 1 ? ' Days ' : ' Day ');

                                          echo '<br>or<br>';
                                          echo $cd_in_months . ($cd_month > 1 ? ' Months' : ' Month');
                                       ?>
                                 </td>
                           </tr>
                        </tbody>
                    </table>
                </div>
                <div class="panel-content">                                
                    <table class="table">
                        <tbody>                           
                           <tr>
                                 <th class="text-left">Average monthly savings:</th>
                                 <td class="text-right">&#8369;&nbsp;<?php echo number_format($average_savings ,2); ?></td>
                           </tr>
                           <tr>
                                 <th class="text-left">Total savings:</th>
                                 <td class="text-right">&#8369;&nbsp;<?php echo number_format($total_savings ,2); ?></td>
                           </tr>
                           <tr>
                                 <th class="text-left">Expected savings until target date:</th>
                                 <?php 
                                       $projected_savings = $average_savings * $cd_in_months;
                                 ?>
                                 <td class="text-right">&#8369;&nbsp;<?php echo number_format($projected_savings, 2); ?></td>
                           </tr>
                           <tr>
                                 <th class="text-left">Total projected savings:</th>
                                 <?php 
                                       $projected_savings = ($average_savings * $cd_in_months) + $total_savings;
                                 ?>
                                 <td class="text-right">&#8369;&nbsp;<?php echo number_format($projected_savings, 2); ?></td>
                           </tr>
                           <tr>
                                 <th class="text-left">Total Withdrawals:</th>
                                 <td class="text-right">&#8369;&nbsp;<?php echo number_format($getSumAllWithdrawals ,2); ?></td>
                           </tr>                           
                           <tr>
                                 <th class="text-left">Total Projected Savings less withdrawals:</th>
                                 <?php 
                                       $projected_savings_less_withdrawals = (($average_savings * $cd_in_months) + $total_savings) - $getSumAllWithdrawals;
                                 ?>
                                 <td class="text-right">&#8369;&nbsp;<?php echo number_format($projected_savings_less_withdrawals, 2); ?></td>
                           </tr>                           
                        </tbody>
                    </table>
               </div>
            </div>
         </div>
         <div class="col-md-8">            
            <div class="panel">
               <div class="panel-heading">
                  <h3 class="panel-title">
                     Transactions
                  </h3>
               </div>
               <div class="panel-content">
                  <?php          
                     if($savings) 
                     { 
                        $column_total = array(); ?>
                        <div class="table-responsive">
                           <table class='table'>
                              <thead>
                                 <tr>
                                    <th>#</th>
                                    <th class='d-none d-sm-table-cell'>Date</th>                                    
                                    <th>Transaction</th>                                    
                                    <th class='d-none d-sm-table-cell'>Bank</th>
                                    <th class='d-none d-sm-table-cell'>User</th>                                    
                                    
                                    <th class='text-right'>Amount</th>
                                    <th class='text-right'>Balance</th>                                    
                                    <th class='d-none d-sm-table-cell'>Remarks</th>                                    
                                 </tr>
                              </thead>
                              <tbody>
                                 <?php
                                 $total_amount = 0;
                                 foreach ($savings as $s => $save) {

                                    $amount = $save['amount'];
                                    $bank_name = $save['bank_name'];
                                    $type = $save['type'];
                                    $user = $save['user'];
                                    $remarks = $save['remarks'];

                                    $date = date_create($save['date']);                               
                                    echo "<tr>";
                                    echo      "<td>". ++$s ."</td>";                                                          
                                    echo      "<td class='d-none d-sm-table-cell'>". date_format($date,"M d, Y") ."</td>";
                                    echo      "<td class=''>". $type .
                                                    (isset($remarks) ? "<span class='d-block d-sm-none text-subtext'><small class='fas fa-info'></small> " . $remarks : '') . "</span>".
                                                    "<span class='d-block d-sm-none text-subtext'><small class='fas fa-calendar'></small> " . date_format($date,"M d, Y") . "</span>
                                                    <span class='d-block d-sm-none text-subtext'><small class='fas fa-university'></small> " . $bank_name . "</span>
                                                    <span class='d-block d-sm-none text-subtext'><small class='fas fa-user'></small> " . $user . "</span>
                                                </td>";
                                    echo      "<td class='d-none d-sm-table-cell'>". $bank_name ."</td>";
                                    echo      "<td class='d-none d-sm-table-cell'>". $user ."</td>";                                    
                                    if($type == 'deposit') {
                                        echo "<td class='text-right'>". number_format($amount) ."</td>";
                                        $total_amount += $amount;
                                    } else {
                                        echo "<td class='text-right'>(". number_format($amount) .")</td>";                                        
                                        $total_amount -= $amount;
                                    }
                                    echo "<td class='text-right'>". number_format($total_amount) ."</td>";                                    
                                    echo "<td class='d-none d-sm-table-cell'>". $remarks ."</td>";                                    
                                    echo "</tr>";
                                 }
                                 ?>
                              </tbody>
                              <tfoot>
                                 <tr>
                                    <th colspan="7" class='text-right'> &#8369;<?php echo number_format($total_amount); ?> </th>
                                 </tr>
                              </tfoot>
                           </table>
                        </div>
                  <?php 
                     } ?>
               </div>
            </div>
         </div>         
      </div>
   </div>
</div>
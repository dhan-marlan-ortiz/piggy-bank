<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!doctype html>
<html lang="en">

<head>
   <!-- favicon -->
   <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url('assets/images/favicon/apple-icon-57x57.png'); ?>">
   <link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url('assets/images/favicon/apple-icon-60x60.png'); ?>">
   <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url('assets/images/favicon/apple-icon-72x72.png'); ?>">
   <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('assets/images/favicon/apple-icon-76x76.png'); ?>">
   <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url('assets/images/favicon/apple-icon-114x114.png'); ?>">
   <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url('assets/images/favicon/apple-icon-120x120.png'); ?>">
   <link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url('assets/images/favicon/apple-icon-144x144.png'); ?>">
   <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url('assets/images/favicon/apple-icon-152x152.png'); ?>">
   <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url('assets/images/favicon/apple-icon-180x180.png'); ?>">
   <link rel="icon" type="image/png" sizes="192x192" href="<?php echo base_url('assets/images/favicon/android-icon-192x192.png'); ?>">
   <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url('assets/images/favicon/favicon-32x32.png'); ?>">
   <link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url('assets/images/favicon/favicon-96x96.png'); ?>">
   <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('assets/images/favicon/favicon-16x16.png'); ?>">
   <link rel="manifest" href="<?php echo base_url('assets/images/favicon/manifest.json'); ?>">
   <!-- favicon end -->

   <meta name="msapplication-TileColor" content="#ffffff">
   <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
   <meta name="theme-color" content="#ffffff">
   
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
   <title>PiggyBank | <?php echo $title; ?></title>
   <link rel="stylesheet" href="<?php echo base_url('assets/css/main.css'); ?>">
</head>

<body>

<nav>
    <div class="container">
        <div class="nav-icon">
        <img src="<?php echo base_url('assets/images/favicon/android-icon-36x36.png'); ?>" alt="PiggyBank"
            draggable="false">
        </div>
        <div class="nav-title">
            <a href="<?php echo base_url('Savings'); ?>">Piggy<span>Bank</span></a>
        </div>
        <div class="nav-user">
            <p>
                <i class="fas fa-user-circle"></i> <?php echo $this->session->userdata('name'); ?>
            </p>
        </div>
    </div>
</nav>

<div class="container">    
    <h3 class="d-inline-block">
        <?php echo $title; ?>
    </h3>
    <ul class="breadcrumbs float-right">
        <li>
            <a href="<?php echo base_url('index.php/Savings'); ?>">Savings</a>
        </li>        
        <li>
            <a href="<?php echo base_url('index.php/Transactions'); ?>">Transactions</a>
        </li>
        <li>
            <a href="<?php echo base_url('index.php/Accounts'); ?>">Bank Accounts</a>
        </li>
        <?php if(!$this->session->userdata('id')) { ?>
            <li>
                <a href="<?php echo base_url('index.php/Login'); ?>">Login</a>
            </li>
        <?php } ?>
        <li>
            <a href="<?php echo base_url('index.php/Logout'); ?>">Logout</a>
        </li>        
    </ul>
</div>
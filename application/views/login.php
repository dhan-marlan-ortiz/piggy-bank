<?php
     defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html lang="en">

<head>
     <!-- favicon -->
     <link rel="apple-touch-icon" sizes="57x57"
          href="<?php echo base_url('assets/images/favicon/apple-icon-57x57.png'); ?>">
     <link rel="apple-touch-icon" sizes="60x60"
          href="<?php echo base_url('assets/images/favicon/apple-icon-60x60.png'); ?>">
     <link rel="apple-touch-icon" sizes="72x72"
          href="<?php echo base_url('assets/images/favicon/apple-icon-72x72.png'); ?>">
     <link rel="apple-touch-icon" sizes="76x76"
          href="<?php echo base_url('assets/images/favicon/apple-icon-76x76.png'); ?>">
     <link rel="apple-touch-icon" sizes="114x114"
          href="<?php echo base_url('assets/images/favicon/apple-icon-114x114.png'); ?>">
     <link rel="apple-touch-icon" sizes="120x120"
          href="<?php echo base_url('assets/images/favicon/apple-icon-120x120.png'); ?>">
     <link rel="apple-touch-icon" sizes="144x144"
          href="<?php echo base_url('assets/images/favicon/apple-icon-144x144.png'); ?>">
     <link rel="apple-touch-icon" sizes="152x152"
          href="<?php echo base_url('assets/images/favicon/apple-icon-152x152.png'); ?>">
     <link rel="apple-touch-icon" sizes="180x180"
          href="<?php echo base_url('assets/images/favicon/apple-icon-180x180.png'); ?>">
     <link rel="icon" type="image/png" sizes="192x192"
          href="<?php echo base_url('assets/images/favicon/android-icon-192x192.png'); ?>">
     <link rel="icon" type="image/png" sizes="32x32"
          href="<?php echo base_url('assets/images/favicon/favicon-32x32.png'); ?>">
     <link rel="icon" type="image/png" sizes="96x96"
          href="<?php echo base_url('assets/images/favicon/favicon-96x96.png'); ?>">
     <link rel="icon" type="image/png" sizes="16x16"
          href="<?php echo base_url('assets/images/favicon/favicon-16x16.png'); ?>">
     <link rel="manifest" href="<?php echo base_url('assets/images/favicon/manifest.json'); ?>">
     <meta name="msapplication-TileColor" content="#ffffff">
     <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
     <meta name="theme-color" content="#ffffff">
     <!-- favicon end -->

     <meta charset="utf-8">
     <meta name="author" content="Dhan Marlan Ortiz">
     <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
     <title>PiggyBank</title>
     <link rel="stylesheet" href="<?php echo base_url('assets/css/main.css'); ?>">
</head>

<body class="login-page" style="background-image: url('<?php echo base_url(); ?>/assets/images/symphony.png')">
     <nav>
          <div class="container">
               <div class="nav-icon">
                    <img src="<?php echo base_url('assets/images/favicon/android-icon-36x36.png'); ?>" alt="PiggyBank"
                         draggable="false">
               </div>
               <div class="nav-title">
                    <a href="<?php echo base_url(); ?>">Piggy<span>Bank</span></a>
               </div>
          </div>
     </nav>
     <div id="login-form">
          <form method="POST" action="<?php echo base_url('index.php/Login'); ?>" autocomplete="off">
               <h2>LOGIN</h2>
               <label for="email">Email Address <i class="danger">*</i></label>
               <input type="text" name="email" id="email" placeholder="Email Address"
                    class="<?php if(form_error('email')) echo 'has-error';?>" value="<?php echo set_value('email'); ?>">
               <label for="password">Password <i class="danger">*</i></label>
               <input type="password" name="password" id="password" placeholder="Password"
                    class="<?php if(form_error('password')) echo 'has-error';?>"
                    value="<?php echo set_value('password'); ?>">
               <input type="submit" name="login" Value="Submit" id="login">
          </form>

          <?php
               if(null !== $this->session->flashdata('error')) {
                    echo '<div class="errors-authentication">
                              <p>' .
                                   $this->session->flashdata('error') .
                              '</p>
                         </div>';
               }

               if(validation_errors()) {
                    echo '<div class="errors-validation">' .
                              validation_errors('') .
                         '</div>';
               }
          ?>

          <div class="notes">
               <p><span class="danger"> *</span> required fields.</p>
          </div>
     </div>


     <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-G6XM6X3R87"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-G6XM6X3R87');
    </script>

</body>

</html>
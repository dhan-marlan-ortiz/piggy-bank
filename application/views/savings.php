<div class="inner-page">   
   <div class="container">
      <!-- Add new saving -->
      <div class="max-width-xs">
         
      </div>
      <!-- Add new saving end -->
   </div>
   <div class="container">
      <div class="row">
         <div class="col-md-4">
            <div class="panel">
               <div class="panel-heading">
                  <h3 class="panel-title">
                     Add Record
                  </h3>
               </div>
               <div class="panel-content">
                  <form method="POST" action="Savings">
                     <div class="">
                        <div class="">
                           <input type="number" id="amount" name="amount" placeholder="Amount" required>
                        </div>
                        <div class="">
                           <input type="date" id="date" name="date" placeholder="Date" value="<?php echo date('Y-m-d'); ?>"
                              required>
                        </div>
                        <div class="">
                           <select name="bank" id="" required="">
                              <option value="" selected disabled>Select Bank</option>
                              <?php 
                                    foreach ($banks as $bank) {
                                          echo "<option value='" . $bank['id'] . "'>" . $bank['bank_name'] . "</option>";
                                    }
                              ?>
                           </select>
                        </div>
                        <div class="">
                           <input type="submit" id="save" name="save" value="Save">
                        </div>
                     </div>
                  </form>
               </div>
            </div>
            
            <?php 
               $total_savings = $getSumAllSavings;
               $month_elapsed =  $getMonthElapse['month_elapse'];
               $average_savings = $total_savings / $month_elapsed;
               $target_date = $getOptions[0]['option_value'];

               $cd_year = date_diff(date_create($target_date), date_create(date('Y-m-d')))->format('%y');
               $cd_month = date_diff(date_create($target_date), date_create(date('Y-m-d')))->format('%m');
               $cd_day = date_diff(date_create($target_date), date_create(date('Y-m-d')))->format('%d');
               $cd_in_months = (intval($cd_year) * 12) + intval($cd_month);
            ?>
            <div class="panel">
               <div class="panel-heading">
                  <h2 class="panel-title">Summary</h2>
               </div>
               <div class="panel-content">
                  <table class="table">
                        <tbody>
                           <tr>
                                 <th class="text-left">First Entry</th>
                                 <td class="text-right">
                                       <?php echo date("M d, Y",strtotime($getMonthElapse['date_first'])); ?>
                                 </td>
                           </tr>
                           <tr>
                                 <th class="text-left">Last Entry</th>
                                 <td class="text-right">
                                       <?php echo date("M d, Y",strtotime($getMonthElapse['date_last'])); ?>
                                 </td>
                           </tr>
                           <tr>
                                 <th class="text-left">Months</th>
                                 <td class="text-right">
                                       <?php echo $month_elapsed; ?>
                                 </td>
                           </tr>
                           <tr>
                                 <th class="text-left">Average Monthly Savings:</th>
                                 <td class="text-right">&#8369; <?php echo number_format($average_savings ,2); ?></td>

                           </tr>
                           <tr>
                                 <th class="text-left">Total Savings:</th>
                                 <td class="text-right">&#8369; <?php echo number_format($total_savings ,2); ?></td>
                           </tr>

                           <tr>
                                 <th class="text-left">Projected Savings until target date:</th>
                                 <?php 
                                       $projected_savings = $average_savings * $cd_in_months;
                                 ?>
                                 <td class="text-right">&#8369; <?php echo number_format($projected_savings, 2); ?></td>
                           </tr>
                           <tr>
                                 <th class="text-left">Total Projected Savings:</th>
                                 <?php 
                                       $projected_savings = ($average_savings * $cd_in_months) + $total_savings;
                                 ?>
                                 <td class="text-right">&#8369; <?php echo number_format($projected_savings, 2); ?></td>
                           </tr>
                           <tr>
                                 <th class="text-left">Target Date</th>
                                 <td class="text-right">

                                       <?php //echo $getOptions['target_date'][];
                                          echo date("M d, Y",strtotime($target_date));
                                       ?>
                                 </td>
                           </tr>
                           <tr>
                                 <th class="text-left">Countdown</th>
                                 <td class="text-right">
                                       <?php 
                                          
                                          echo $cd_year . " Year, " . $cd_month . ($cd_month > 1 ? ' Months, ' : ' Month, ') . $cd_day . ($cd_month > 1 ? ' Days ' : ' Day ');

                                          echo '<br>or<br>';
                                          echo $cd_in_months . ($cd_month > 1 ? ' Months' : ' Month');
                                       ?>
                                 </td>
                           </tr>
                        </tbody>
                  </table>
               </div>
            </div>
         </div>
         <div class="col-md-8">            
            <div class="panel">
               <div class="panel-heading">
                  <h3 class="panel-title">
                     Bank Deposits
                  </h3>
               </div>
               <div class="panel-content">
                  <?php          
                     if($savings) 
                     { 
                        $column_total = array(); ?>
                        <div class="table-responsive">
                           <table class='table'>
                              <thead>
                                 <tr>
                                    <th>#</th>
                                    <th>
                                       Date
                                       <span class='d-inline-block d-sm-none'>& Bank</span>
                                    </th>
                                    <th class='d-none d-sm-table-cell'>Bank</th>
                                    <?php foreach ($account_users as $au => $a_users) {
                                                                  echo "<th class='text-center'>" . $a_users['name'] . "</th>";
                                                                  array_push($column_total, array('user_id' => $a_users['id'], 'amount' => 0));
                                                            } ?>
                                    <th class='text-center'>Total</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <?php
                                 $total_amount = 0;
                                 foreach ($savings as $s => $save) {

                                    $amount = $save['amount'];
                                    $bank_name = $save['bank_name'];

                                    $date = date_create($save['date']);
                                    $total_amount += $save['amount'];

                                    echo "<tr>";
                                    echo      "<td>". ++$s ."</td>";                              
                                    echo      "<td>
                                                   <span>". date_format($date,"M d, Y") . "</span>
                                                   <span class='d-block d-sm-none mt-5'>". $bank_name . "</span>
                                                </td>";

                                    echo      "<td class='d-none d-sm-table-cell'>". $bank_name ."</td>";
                                    foreach ($account_users as $au => $a_users) {
                                          if($a_users['id'] == $save['user']) {
                                                echo "<td class='text-right'>". number_format($amount) ."</td>";

                                                foreach ($column_total as $ct => $c_total) {
                                                   if($c_total['user_id'] == $save['user']) {
                                                         $column_total[$ct]['amount'] += $amount;
                                                   }
                                                }

                                          } else {
                                                echo "<td></td>";
                                          }
                                    }
                                    echo      "<td class='text-right'>". number_format($total_amount) ."</td>";
                                    echo "</tr>";
                                 }
                                 ?>
                              </tbody>
                              <tfoot>
                                 <tr>
                                    <th></th>
                                    <th></th>
                                    <th class='d-none d-sm-table-cell'></th>
                                    <?php 
                                    foreach ($account_users as $au => $a_users) {
                                       foreach ($column_total as $ct => $c_total) {
                                             if($c_total['user_id'] == $a_users['id']) {
                                                   echo "<th class='text-right'>&#8369;" . number_format($c_total['amount']) . "</th>";
                                             }
                                       }
                                    } 
                                    ?>
                                    <th class='text-right'> &#8369;<?php echo number_format($total_amount); ?> </th>
                                 </tr>
                              </tfoot>
                           </table>
                        </div>
                  <?php 
                     } ?>
               </div>
            </div>
         </div>         
      </div>
   </div>
</div>
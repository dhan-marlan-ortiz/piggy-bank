     <div class="inner-page">          
          <div class="container">
               <?php 
                    $total_savings = $getSumAllSavings;
                    $month_elapsed =  $getMonthElapse['month_elapse'];
                    $average_savings = $total_savings / $month_elapsed;
                    $target_date = $getOptions[0]['option_value'];

                    $cd_year = date_diff(date_create($target_date), date_create(date('Y-m-d')))->format('%y');
                    $cd_month = date_diff(date_create($target_date), date_create(date('Y-m-d')))->format('%m');
                    $cd_day = date_diff(date_create($target_date), date_create(date('Y-m-d')))->format('%d');
                    $cd_in_months = (intval($cd_year) * 12) + intval($cd_month);
               ?>
               <table class="table max-width-xs">
                    <tbody>
                         <tr>
                              <th class="text-left">First Entry</th>
                              <td class="text-right">
                                   <?php echo date("M d, Y",strtotime($getMonthElapse['date_first'])); ?>
                              </td>
                         </tr>
                         <tr>
                              <th class="text-left">Last Entry</th>
                              <td class="text-right">
                                   <?php echo date("M d, Y",strtotime($getMonthElapse['date_last'])); ?>
                              </td>
                         </tr>
                         <tr>
                              <th class="text-left">Months</th>
                              <td class="text-right">
                                   <?php echo $month_elapsed; ?>
                              </td>
                         </tr>
                         <tr>
                              <th class="text-left">Average Monthly Savings:</th>
                              <td class="text-right">&#8369; <?php echo number_format($average_savings ,2); ?></td>

                         </tr>
                         <tr>
                              <th class="text-left">Total Savings:</th>
                              <td class="text-right">&#8369; <?php echo number_format($total_savings ,2); ?></td>
                         </tr>

                         <tr>
                              <th class="text-left">Projected Savings until target date:</th>
                              <?php 
                                   $projected_savings = $average_savings * $cd_in_months;
                              ?>
                              <td class="text-right">&#8369; <?php echo number_format($projected_savings, 2); ?></td>
                         </tr>
                         <tr>
                              <th class="text-left">Total Projected Savings:</th>
                              <?php 
                                   $projected_savings = ($average_savings * $cd_in_months) + $total_savings;
                              ?>
                              <td class="text-right">&#8369; <?php echo number_format($projected_savings, 2); ?></td>
                         </tr>
                         <tr>
                              <th class="text-left">Target Date</th>
                              <td class="text-right">

                                   <?php //echo $getOptions['target_date'][];
                                        echo date("M d, Y",strtotime($target_date));
                                   ?>
                              </td>
                         </tr>
                         <tr>
                              <th class="text-left">Countdown</th>
                              <td class="text-right">
                                   <?php 
                                        
                                        echo $cd_year . " Year, " . $cd_month . ($cd_month > 1 ? ' Months, ' : ' Month, ') . $cd_day . ($cd_month > 1 ? ' Days ' : ' Day ');

                                        echo '<br>or<br>';
                                        echo $cd_in_months . ($cd_month > 1 ? ' Months' : ' Month');
                                   ?>
                              </td>
                         </tr>
                    </tbody>
               </table>
          </div>
     </div>


    <script src="https://kit.fontawesome.com/1ee9e7de96.js" crossorigin="anonymous"></script>
    
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-G6XM6X3R87"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-G6XM6X3R87');
    </script>
</body>
</html>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct() {
		 parent::__construct();
	}

	public function index() {
		if(null !== $this->input->post('login')) {
			$this->load->model('Accounts_model');
			$email = $this->input->post('email');
			$password = $this->input->post('password');

			// form validation start
			$validation_rules = array(
				array(
					'field' => 'email',
					'label' => 'email',
					'rules' => array(
							'required',
							'valid_email'
							),
					'errors' => array(
							'required' => '%s is required.',
							'valid_email' => 'Please provide a valid %s'
							)
				),
				array(
					'field' => 'password',
					'label' => 'password',
					'rules' => array(
							'required'
							),
					'errors' => array(
							'required' => '%s is required.'
						)
				)
			);

			$this->form_validation->set_rules($validation_rules);
			if ($this->form_validation->run() == FALSE) {
				$this->load->view('login');
			} else {
				// validation true
				// authentication start
				$authentication = $this->Accounts_model->loginAuthentication($email, $password);
				if(null !== $authentication) {

					$this->load->library('session');
					$user = array(
						'name'  => $authentication['name'],
						'email'     => $authentication['email'],
						'id' => $authentication['id'],
						'account' => $authentication['account_id']
					);

					$this->session->set_userdata($user);
					header('Location: ' . base_url('index.php/Savings'));
				}else {
					$this->session->set_flashdata('error', 'Invalid username or password.');
					$this->load->view('login');
				}
				// authentication end
			}



		} else {
			$this->load->view('login');
		}
	}
}

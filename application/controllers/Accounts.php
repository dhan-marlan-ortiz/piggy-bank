<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Accounts extends CI_Controller {

	public function __construct() {
        parent::__construct();
		$this->load->library('session');
		$this->load->model('Savings_model');

        if(!$this->session->userdata('id')) {
			 $this->session->set_flashdata('error', 'Forbidden Access.');
			 header('Location: ' . base_url('index.php/Login'));
		}
	}

	public function index() {		
		/*
		if(null !== $this->input->post('save')) {
			$this->Savings_model->addTransaction();
			header('Location: ' . base_url('Transactions?save=1'));
		}else {
			
			
		}
		*/
		// $data['savings'] = $this->Savings_model->getTransactions();
		// $data['banks'] = $this->Savings_model->getBanks();
		// $data['transaction_types'] = $this->Savings_model->getTransactionTypes();
		// $account_id = $data['savings'][0]['user'];
		// $data['account_users'] = $this->Savings_model->getAccountUsers($account_id);
		// $data['savings_dhan'] = $this->Savings_model->getTotalSavings(1);
		// $data['savings_che'] = $this->Savings_model->getTotalSavings(2);
		
		$data['getSumAllSavings'] = $this->Savings_model->getSumAllSavings();
		// $data['getSumAllWithdrawals'] = $this->Savings_model->getSumAllWithdrawals();
		$data['getMonthElapse'] = $this->Savings_model->getMonthElapse();
		$data['getOptions'] = $this->Savings_model->getOptions();

		
		$data['banks'] = $this->Savings_model->getBanks();
		
		$data['title'] = "Bank Accounts";
		$this->load->view('inner-page-header', $data);
		$this->load->view('banks', $data);
		$this->load->view('inner-page-footer');
	}

	public function bank($bank_id) {

	}

}

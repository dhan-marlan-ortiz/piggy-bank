<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Savings extends CI_Controller {

	public function __construct() {
        parent::__construct();
		$this->load->library('session');
		$this->load->model('Savings_model');

        if(!$this->session->userdata('id')) {
			 $this->session->set_flashdata('error', 'Forbidden Access.');
			 header('Location: ' . base_url('index.php/Login'));
		}
     }

	public function index() {
		if(null !== $this->input->post('save')) {
			$this->Savings_model->addSaving();
			header('Location: ' . base_url('index.php/Savings?save=1'));
		}else {
			$data['savings'] = $this->Savings_model->getSavings();
			$data['banks'] = $this->Savings_model->getBanks();
			$account_id = $data['savings'][0]['user'];
			$data['account_users'] = $this->Savings_model->getAccountUsers($account_id);
			// $data['savings_dhan'] = $this->Savings_model->getTotalSavings(1);
			// $data['savings_che'] = $this->Savings_model->getTotalSavings(2);
			$data['getSumAllSavings'] = $this->Savings_model->getSumAllSavings();
			$data['getMonthElapse'] = $this->Savings_model->getMonthElapse();
			$data['getOptions'] = $this->Savings_model->getOptions();
			$data['title'] = "Savings";
			$this->load->view('inner-page-header', $data);
			$this->load->view('savings', $data);
			$this->load->view('inner-page-footer');
		}

	}

	public function report() {
		$data['getSumAllSavings'] = $this->Savings_model->getSumAllSavings();
		$data['getMonthElapse'] = $this->Savings_model->getMonthElapse();
		$data['getOptions'] = $this->Savings_model->getOptions();
		
		$data['title'] = "Report";
		$this->load->view('inner-page-header', $data);
		$this->load->view('report', $data);
		$this->load->view('inner-page-footer');
	}
}
